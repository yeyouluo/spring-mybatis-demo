package com.yeyouluo.demo.spring.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.yeyouluo.demo.spring.dao.IServiceDao;
import com.yeyouluo.demo.spring.pojo.Service;

@Repository
public class ServiceDao implements IServiceDao {

	private static final String NAMESPACE = "com.yeyouluo.demo.spring.dao.IServiceDao.";
	
	@Resource
	private SqlSessionTemplate template;
	
	@Override
	public List<Service> getAllServices() {
		return template.selectList(NAMESPACE + "getAllServices");
	}

	@Override
	public void addService(Service service) {
		template.insert(NAMESPACE + "addService", service);
	}

}
