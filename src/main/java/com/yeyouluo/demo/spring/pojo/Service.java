package com.yeyouluo.demo.spring.pojo;

public class Service {
	private String code;
	private String name;
	private String remarks;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Service [code=" + code + ", name=" + name + ", remarks=" + remarks + "]";
	}

}
