package com.yeyouluo.demo.spring.dao;

import java.util.List;

import com.yeyouluo.demo.spring.pojo.Service;

public interface IServiceDao {
	public List<Service> getAllServices();
	public void addService(Service service);
}
