package com.yeyouluo.demo.spring.dao.test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yeyouluo.demo.spring.dao.impl.ServiceDao;
import com.yeyouluo.demo.spring.pojo.Service;

public class ServiceDaoTest {
	
	@SuppressWarnings("resource")
	@Test
	public void test1(){
		String config = "/spring-base.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		ServiceDao dao = ac.getBean("serviceDao",ServiceDao.class);
		List<Service> services = dao.getAllServices();
		for( Service s : services ) {
			System.out.println( s.toString() );
		}
	}
		
		@Test
		public void test2(){
			String config = "/spring-base.xml";
			ApplicationContext ac = new ClassPathXmlApplicationContext(config);
			ServiceDao dao = ac.getBean("serviceDao",ServiceDao.class);
			Service service = new Service();
			service.setCode("com.yeyouluo.test");
			dao.addService(service);
		
	}
}
